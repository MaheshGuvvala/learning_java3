
public class CheckDiscount 
{
	public static void main(String[] args) 
	{
	int coffees_ordered_in_last_week = 6;
	
	int discount = 0;
	
	if (coffees_ordered_in_last_week > 5)
	{
		discount = 10;
	}
	System.out.println("This customer is eligible for a " + discount + "% discount.");
	}

}
