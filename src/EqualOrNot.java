import java.util.Scanner;

public class EqualOrNot 
{
	public static void main(String[]args) 
	{
		System.out.println("This is a program to find if 2 numbers are equal");
		
		
		Scanner input = new Scanner(System.in);
		System.out.println("Please enter the first number");
		
		int firstNumber = input.nextInt();
		System.out.println("Please enter the second number");
		
		int secondNumber = input.nextInt();
		System.out.println("Are two numbers equal ? " + (firstNumber == secondNumber));
	}

}
