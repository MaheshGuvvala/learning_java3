import java.util.Scanner;

public class SumOfFirstAndLast 
{
	 public static void main(String[]args) 
	 {

	        int r, n, rev = 0;
	         int fd, ld, sum;

	        Scanner input = new Scanner(System.in);
	        System.out.print("Enter a number:");
	        n = input.nextInt();
	        
	        ld = n % 10;

	        while (n > 0) 
	        {
	            r = n % 10;
	            rev = rev * 10 + r;
	            n = n / 10;
	        }

	        fd = rev % 10;
	        sum = fd + ld;

	        System.out.println("Sum of first and last digits:" + sum);

	    }

}
