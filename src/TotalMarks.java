
public class TotalMarks 
{
	 public static void main(String[] args)
	    {
	        int N = 3, totalmarks = 0;
	        float percentage;
	  
	        // create 1-D array to store marks
	        int marks[] = {78,45,62};
	  
	        // calculate total marks
	        for (int i = 0; i < N; i++) 
	        {
	            totalmarks += marks[i];
	        }
	        System.out.println("Total Marks : " + totalmarks);
	  
	        // calculate percentage
	        
	        percentage = (totalmarks / (float)N);
	        System.out.println("Total Percentage : " + percentage + "%");
	    }


}
